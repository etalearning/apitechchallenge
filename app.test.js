const request = require("supertest");
const app = require("./app");

describe("API Endpoints", () => {
  describe("Endpoint - /", () => {
    it("returns nothing", async () => {
      const res = await request(app).get("/");
      expect(res.statusCode).toEqual(200);
      expect(res.body).toBeFalsy();
    });
  });

  describe("Endpoint - /health", () => {
    it("returns a json object with appName, appVersion and gitHash", async () => {
      const res = await request(app).get("/health");
      expect(res.statusCode).toEqual(200);
      expect(res.body).toHaveProperty("appName");
      expect(res.body).toHaveProperty("appVersion");
      expect(res.body).toHaveProperty("gitHash");
    });
  });
});
