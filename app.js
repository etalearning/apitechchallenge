const express = require("express");
const app = express();
const packageJson = require("./package.json");
const axios = require("axios");

const gitlabUrl =
  "https://gitlab.com/api/v4/projects/31089646/repository/commits";

app.get("/", async (req, res) => {
  res.json();
});

app.get("/health", async (req, res) => {
  const gitlabCommits = await axios.get(gitlabUrl);

  let result = {
    appName: packageJson.description,
    appVersion: packageJson.version,
    gitHash: gitlabCommits.data.map((commit) => ({
      id: commit.id,
      message: commit.title,
    })),
  };

  res.json(result);
});

module.exports = app;
