const app = require("./app");
const port = 3000;

app.listen(port, () => {
  console.log(`Bendigo Tech Challenge listening at http://localhost:${port}`);
});
