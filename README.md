## Bendigo Tech Challenge

- URL: https://bendigo-gpdippvm3q-uc.a.run.app/health
- Output:
  ```json
  {
    "appName": "Bendigo Tech Challenge",
    "appVersion": "1.1.0",
    "gitHash": [
        {
            "id": "6ecc555dfaa0899b6f5b6f2f09c40cf5abdd2ea3",
            "message": "refactoring"
        },
        ...
        {
            "id": "9ba968e2ef6c2ab04ed091f9a5a9950f07ef487f",
            "message": "Tech Challenge Project init"
        }
    ]
  }
  ```

## How to Run

### Local
```bash
git clone git@gitlab.com:etalearning/apitechchallenge.git
cd apitechchallenge
docker-compose build
docker-compose up -d
curl http://localhost:3000/health
```

- unit testing: `npm test`
- [local automation](https://gitlab.com/etalearning/apitechchallenge/-/issues/3#note_725778676)

### Cloud
```
curl https://bendigo-gpdippvm3q-uc.a.run.app/health
```

- [Last Passed Gitlab Pipeline](https://gitlab.com/etalearning/apitechchallenge/-/pipelines/403738385) - the pipeline will fail if the local gitlab-runner is offline
- Docker image hosted on Google Container Registry
- Docker container running on Google Cloud Run
